<?php include('./header.php');?>

<!-- Team Members START -->
<div class="section-block-grey">
  <div class="container">
    <div class="section-heading center-holder">
      <h3>Team</h3>
      <p>We are first and foremost a team of enthusiasts,</p>
      <p>and we are delighted to be able to provide you with a
         complete tool like this.</p>
    </div>
    <div class="row mt-50  justify-content-md-center">
      <div class="col-md-3 col-sm-6 col-12">
        <div class="team-member">
          <div class="team-member-img">
            <img src="img/team/kaisen.jpg" alt="img">
          </div>
          <div class="team-member-text">
            <h4>Kevin Chevreuil<br><small>Kaisen</small></h4>
            <span>Founder & team leader</span>
            <p>Linux sysadmin, he is the main developer of Kaisen Linux, blog and documentation editor. 
               He also takes care of the putting into production of updates of packages and repositories.</p>
            <ul>
              <li><a href="https://www.linkedin.com/in/kevinchevreuil/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
      </div>

      <div class="row mt-50  justify-content-md-center">
      <div class="col-md-3 col-sm-6 col-12">
        <div class="team-member">
          <div class="team-member-img">
            <img src="img/team/labrute.jpg" alt="img">
          </div>
          <div class="team-member-text">
            <h4>Mehdi Lecomte<br><small>LaBrute!</small></h4>
            <span>Core developer</span>
            <p>Linux sysadmin, he is the core developer of Kaisen Linux. Package and infrastructure maintainer,
            He is also in charge of user support. He also participates in the decision making on the evolution of Kaisen.<p>
            <ul>
              <li><a href="https://www.linkedin.com/in/b12644a6/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-sm-6 col-12">
        <div class="team-member">
          <div class="team-member-img">
            <img src="img/team/tobas.jpg" alt="img">
          </div>
          <div class="team-member-text">
            <h4>Tomi Bequet<br><small>tobas</small></h4>
            <span>Graphic designer</span>
            <p>Freelance graphic designer for several years, he provides us with our wallpapers as well as his 
               expertise on the user experience. He is the one who reworked our official logo.</p>

            <ul>
              <li><a href="https://www.linkedin.com/in/tomibequet/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
            <div class="col-md-3 col-sm-6 col-12">
        <div class="team-member">
          <div class="team-member-img">
            <img src="img/contributors/kaisen.jpg" alt="img">
          </div>
          <div class="team-member-text">
            <h4>Arnaud Cormier<br><small>Magiknono</small></h4>
            <span>Manpages maintainer</span>
            <p>Freelance Linux sysadmin and webdev. He contributed a lot on Kaisen Linux
               with videos. Today, he is maintainer of <a href="manpages/">manpages</a> documentation.</p>

            <ul>
              <li><a href="https://www.linkedin.com/in/arnaudcormier/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Team Members END -->
<?php include('./footer.php');?>
